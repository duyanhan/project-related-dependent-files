'#'：用于编写注释

FROM：用于表示从哪个基础镜像的基础上进行构建

MAINTAINER：表示维护这个Dockerfile的作者等信息

RUN：表示在镜像上要执行的命令

yum update：用于更新系统

passwd：用于更新用户的身份验证令牌（口令/密码）

openssh-server：让远程主机可以通过网络访问sshd服务，开始一个安全shell

openssh-client：功能类似于XShell，可以作为一个客户端连接上openssh-server

initscripts：这个库包含用户在各种操作系统下用来运行监控程序的脚本。
http://www.github.com/Supervisor/initscripts

net-tools：起源于BSD的TCP/IP工具箱，后来成为老版本Linux内核中配置网络功能的工具

setuptools：是Python distutils增强版的集合，它可以帮助我们更简单的创建和分发Python包，尤其是拥有依赖关系的。功能亮点是利用EasyInstall自动查找、下载、安装、升级依赖包

supervisor：监控程序
https://github.com/supervisor/supervisor

easy_install supervisor：使用python-setuptools -y来安装supervisor，因为默认的yum安装不了 supervisor

chpasswd：是批量更新用户口令的工具，“echo 用户名:密码 | chpasswd”实现非交互的形式修改密码，这里echo 'root:root'|chpasswd 表示修改容器中的root用户的密码为root

ssh-keygen：用于生成ssh 密钥

EXPOSE命令：只是声明了容器应该打开的端口并没有实际上将它打开，在使用容器的时候，还是需要用-p或-P参数配合端口来映射使用的，这里的“EXPOSE 22”表示当前镜像对应的容器声明了应该打开的端口为22，但实际打开22端口需要运维人员手动指定-p或-P参数

CMD 指令：就是用于指定默认的容器主进程的启动命令的